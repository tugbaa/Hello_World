<?php
require 'vendor/autoload.php';

$app = new \Slim\Slim();

$app->get('/', function () {
    echo "Hello worLd";
});

$app->get('/:val', function ($val) {
    echo "Hello $val";
});

$app->run();